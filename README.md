# Architecture and technology used:

## Clean Code development recommendations
https://habr.com/post/424051/

## Code guidelines
Always use Boy Scout Rule: Leave code better than you found it.

### PHP code guidelines:
PSR-1 https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md
PSR-2 https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md

- Whatever could be moved to config file, please move it. URLs for instance.
- Handle internal library errors.
- Pick class names reasonably.
- Don't leave empty methods.
- Follow single coding style.
- Don't put JS code into view.
- Don’t leave commented out code

### Merge request branch creation

When you have finished writing code and want to commit:

- Create a new branch feature/TICKET-123_handle_order_loading

Where "TICKET-123" is the task/bug name and number and "handle order loading" is the title.
Don't forget that you need to branch off the current "develop" branch.

### Commit comments

Use the CHANGELOG notation https://keepachangelog.com/ru/1.0.0/ for marking [ADDED], [CHANGED],
[REMOVED] or [FIXED] code.

Use the following template for commit messaging:
```
TICKET-123 - handle order loading
[ADDED] OrderInteractor for order downloading
[CHANGED] file downloading via CDN
[REMOVED] ProfileActivity
[FIXED] image caching on disk
````

### Merge request reviewers

After you have created your feature/JIRA-123_add_avatar_loading branch from the current develop branch
and pushed it to remote. Tag appropriate people to review your merge request.
